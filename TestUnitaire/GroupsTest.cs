using Microsoft.EntityFrameworkCore;
using Swagger_API_and_RESTful_application.Context;
using Swagger_API_and_RESTful_application.Entity;
using Swagger_API_and_RESTful_application.IRepository;
using Swagger_API_and_RESTful_application.Repository;
using System;
using System.Threading.Tasks;
using Xunit;

namespace TestUnitaire
{
    public class GroupsTest
    {
        private IGenericRepository<Groups> repository;
       
        #region Context
        public async Task<TestDbContext> GetDatabaseContext()
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).Options;
            var databaseContext = new TestDbContext(options);
            databaseContext.Database.EnsureCreated();
           
            if (await databaseContext.groups.CountAsync() <= 0)
            {
                databaseContext.groups.AddRange(
                    new Groups()
                    {
                        GroupsId = new Guid("0BF77482-2866-4C4D-BD8B-E4EBC67901FD"),
                        Name = "Group1",
                      
                    } );
               
                databaseContext.user.Add(
                    new User()
                    {
                        UserId = new Guid("00000082-2866-4C4D-BD8B-E4EBC67901FD"),
                        Email = "Alii.rabhii@gmail.com",
                        Password = "rabhi123",
                        Name = "Ali",
                        Group = new Groups()
                        {
                            GroupsId = new Guid("111E222B-2866-4C4D-BD8B-E4EBC67901FD"),
                            Name = "Group2",

                        } ,
                        FkGroups = new Guid("111E222B-2866-4C4D-BD8B-E4EBC67901FD")

                    });

                await databaseContext.SaveChangesAsync();
               
            }
            return databaseContext;
        }
        #endregion

        #region Test Read Group
        [Fact]
        public void Should_Return_Group_When_Calling_GetListFileTypes()
        {

            //Arrange
            var dbContext = GetDatabaseContext();
            repository = new GenericRepository<Groups>(dbContext.Result);
            //fileTypeRepository = new FileTypeRepository(dbContext.Result);
            GroupsController controller = new GroupsController(repository);
            //Act
            var group = controller.Get();
            //Assert
            Assert.NotNull(group);
        }
        #endregion


        #region Test Add Function
        [Fact]
        public async Task Given_CreateGroupRequest_When_GroupIsValid_Then_ShouldBeCreatedAndSuccessReturned()
        {
            //Arrange
            Groups group = new Groups()
            {
                Name = "Groupe3",
              
            };
            var dbContext = await GetDatabaseContext();
            repository = new GenericRepository<Groups>(dbContext);

            GroupsController controller = new GroupsController(repository);

            //Act
            var result = controller.Post(group);

            //Assert
            Assert.True(result == "Added Done");

        }
        #endregion

        #region Test Put
        [Fact]
        public async Task Given_UpdateGroupRequest_When_GroupIsValid_Then_ShouldBeUpdateAndSuccessReturned()
        {
            //Arrange
            var _context = await GetDatabaseContext();
            Groups group = _context.groups.FirstOrDefaultAsync().Result;
            group.Name = "GroupUpdated";

            repository = new GenericRepository<Groups>(_context);
            GroupsController controller = new GroupsController(repository);

            //Act
            var result = controller.Put(group);

            //Assert
            Assert.True(result == "Update Done");

        }
        #endregion

        #region Test Delete
        [Fact]
        public async Task Given_DeleteGroupsRequest_When_GroupsIsDeleted_Then_ShouldBeSuccessReturned()
        {
            //Arrange
            var _context = await GetDatabaseContext();
            Groups group = _context.groups.FirstOrDefaultAsync().Result;

            repository = new GenericRepository<Groups>(_context);

            GroupsController controller = new GroupsController(repository);

            //Act
            var result = controller.Delete(group.GroupsId);

            //Assert
            Assert.True(result == "Delete Done");

        }
        #endregion
    

    }
}
