﻿using Microsoft.EntityFrameworkCore;
using Swagger_API_and_RESTful_application.Context;
using Swagger_API_and_RESTful_application.Entity;
using Swagger_API_and_RESTful_application.IRepository;
using Swagger_API_and_RESTful_application.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestUnitaire
{
    public class UserTest
    {
        private IGenericRepository<User> repository;

        #region Context
        public async Task<TestDbContext> GetDatabaseContext()
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).Options;
            var databaseContext = new TestDbContext(options);
            databaseContext.Database.EnsureCreated();

            if (await databaseContext.groups.CountAsync() <= 0)
            {
                databaseContext.groups.AddRange(
                    new Groups()
                    {
                        GroupsId = new Guid("0BF77482-2866-4C4D-BD8B-E4EBC67901FD"),
                        Name = "Group1",

                    });

                databaseContext.user.Add(
                    new User()
                    {
                        UserId = new Guid("00000082-2866-4C4D-BD8B-E4EBC67901FD"),
                        Email = "Alii.rabhii@gmail.com",
                        Password = "rabhi123",
                        Name = "Ali",
                        Group = new Groups()
                        {
                            GroupsId = new Guid("111E222B-2866-4C4D-BD8B-E4EBC67901FD"),
                            Name = "Group2",

                        },
                        FkGroups = new Guid("111E222B-2866-4C4D-BD8B-E4EBC67901FD")

                    });

                await databaseContext.SaveChangesAsync();

            }
            return databaseContext;
        }
        #endregion

        #region Test Read Group
        [Fact]
        public void Should_Return_User_When_Calling_GetListUser()
        {
            //Arrange
            var dbContext = GetDatabaseContext();
            repository = new GenericRepository<User>(dbContext.Result);
            //fileTypeRepository = new FileTypeRepository(dbContext.Result);
            UserController controller = new UserController(repository);
            //Act
            var user = controller.Get();
            //Assert
            Assert.NotNull(user);
        }
        #endregion


        #region Test Add Function
        [Fact]
        public async Task Given_CreateGroupRequest_When_UserIsValid_Then_ShouldBeCreatedAndSuccessReturned()
        {
            //Arrange
            User user = new User()
            {
               
                Email = "User.rabhii@gmail.com",
                Password = "User123",
                Name = "User1",
                Group = new Groups()
                {
                    GroupsId = new Guid("333c666d-2866-4C4D-BD8B-E4EBC67901FD"),
                    Name = "Group5",

                },
                FkGroups = new Guid("333c666d-2866-4C4D-BD8B-E4EBC67901FD")

            };
            var dbContext = await GetDatabaseContext();
            repository = new GenericRepository<User>(dbContext);

            UserController controller = new UserController(repository);

            //Act
            var result = controller.Post(user);

            //Assert
            Assert.True(result == "Added Done");

        }
        #endregion

        #region Test Put
        [Fact]
        public async Task Given_UpdateUserRequest_When_UserIsValid_Then_ShouldBeUpdateAndSuccessReturned()
        {
            //Arrange
            var _context = await GetDatabaseContext();
            User user = _context.user.FirstOrDefaultAsync().Result;
            user.Name = "User2";

            repository = new GenericRepository<User>(_context);
            UserController controller = new UserController(repository);

            //Act
            var result = controller.Put(user);

            //Assert
            Assert.True(result == "Update Done");

        }
        #endregion

        #region Test Delete
        [Fact]
        public async Task Given_DeleteUserRequest_When_UserIsDeleted_Then_ShouldBeSuccessReturned()
        {
            //Arrange
            var _context = await GetDatabaseContext();
            User user = _context.user.FirstOrDefaultAsync().Result;

            repository = new GenericRepository<User>(_context);

            UserController controller = new UserController(repository);

            //Act
            var result = controller.Delete(user.UserId);

            //Assert
            Assert.True(result == "Delete Done");

        }
        #endregion


    }
}
