﻿using Microsoft.EntityFrameworkCore;
using Swagger_API_and_RESTful_application.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swagger_API_and_RESTful_application.Context
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options) {}

        public DbSet<User> user { get; set; }
        public DbSet<Groups> groups { get; set; }
     
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Settings: set Primary keys

            modelBuilder.Entity<User>()
                         .HasKey(c => c.UserId);

            modelBuilder.Entity<Groups>()
                        .HasKey(c => c.GroupsId);

            #endregion

            #region Settings: set one to many relations


            modelBuilder.Entity<Groups>()
                        .HasMany(c => c.Users)
                        .WithOne(e => e.Group)
                        .HasForeignKey(c => c.FkGroups);

            #endregion

        }
    }
}
