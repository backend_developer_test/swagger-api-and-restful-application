﻿using Microsoft.AspNetCore.Mvc;
using Swagger_API_and_RESTful_application.Entity;
using Swagger_API_and_RESTful_application.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swagger_API_and_RESTful_application.Context
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IGenericRepository<User> repository;

        #region Constructor
        public UserController(IGenericRepository<User> repository)
        {
            this.repository = repository;

        }
        #endregion

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return repository.GetList(null, null);
        }


        [HttpPost]
        public string Post(User value)
        {
            return repository.Add(value);
        }


        [HttpPut]
        public string Put(User value)
        {
            return repository.Put(value);
        }


        [HttpDelete("{id}")]
        public string Delete(Guid id)
        {
            return repository.Remove(id);
        }
    }
}
