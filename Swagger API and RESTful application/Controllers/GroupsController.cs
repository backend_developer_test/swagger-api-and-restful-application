﻿using Microsoft.AspNetCore.Mvc;
using Swagger_API_and_RESTful_application.Entity;
using Swagger_API_and_RESTful_application.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Swagger_API_and_RESTful_application.Context
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly IGenericRepository<Groups> repository;
      
        #region Constructor
        public GroupsController(IGenericRepository<Groups> repository)
        {
            this.repository = repository;
   
        }
        #endregion

        
        [HttpGet]
        public IEnumerable<Groups> Get()
        {
            return repository.GetList(null,null);
        }

        
        [HttpPost]
        public string Post( Groups value)
        {
            return repository.Add(value);
        }

       
        [HttpPut]
        public string Put(Groups value)
        {
            return repository.Put(value);
        }

        
        [HttpDelete("{id}")]
        public string Delete(Guid id)
        {
            return repository.Remove(id);
        }
    }
}
